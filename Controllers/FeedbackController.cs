﻿using AutoMapper;
using ByodLauncher.Models;
using ByodLauncher.Models.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ByodLauncher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ByodLauncherContext _context;

        public FeedbackController(
            ByodLauncherContext context,
            IMapper mapper
        )
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<FeedbackDto>>> GetAllFeedbacks()
        {
            return _mapper.Map<List<Feedback>, List<FeedbackDto>>(await _context.Feedbacks.ToListAsync());
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<FeedbackDto>>> GetAllFeedbacks(Guid tutorialTargetId)
        {
            var feedbacksTutorialTarget = await _context.Feedbacks
                    .Where(f => f.TutorialTargetId == tutorialTargetId).ToListAsync();
            return _mapper.Map<List<Feedback>, List<FeedbackDto>>(feedbacksTutorialTarget);
        }

        [HttpPost]
        public async Task<IActionResult> PostFeedback(FeedbackDto feedbackDto)
        {            
            var feedback = _mapper.Map<Feedback>(feedbackDto);
            _context.Feedbacks.Add(feedback);
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}
