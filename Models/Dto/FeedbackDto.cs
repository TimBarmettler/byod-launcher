﻿using ByodLauncher.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ByodLauncher.Models.Dto
{
    public class FeedbackDto
    {
        [JsonConverter(typeof(GuidConverter))] 
        public Guid Id { get; set; }

        public string Comment { get; set; }

        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }

        [Required]
        [JsonConverter(typeof(GuidConverter))]
        public Guid TutorialTargetId { get; set; }
    }
}
