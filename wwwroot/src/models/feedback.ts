﻿import { Id } from "@/models/idType";

export class Feedback {
    id: Id = null;
    comment = '';
    rating = 0;
    tutorialTargetId: Id = null;

    public constructor(init?: Partial<Feedback>) {
        Object.assign(this, init);
    }

    update(updateData: Partial<Feedback>) {
        Object.assign(this, updateData);
    }
}