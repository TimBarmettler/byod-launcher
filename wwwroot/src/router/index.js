"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vue_1 = require("vue");
var vue_router_1 = require("vue-router");
var Home_vue_1 = require("../views/Home.vue");
var NewSession_vue_1 = require("../views/NewSession.vue");
var NewTutorialTarget_vue_1 = require("../views/NewTutorialTarget.vue");
var JoinSession_vue_1 = require("../views/JoinSession.vue");
var AttendSession_vue_1 = require("../views/AttendSession.vue");
var OrchestrateSession_vue_1 = require("@/views/OrchestrateSession.vue");
var EditSession_vue_1 = require("@/views/EditSession.vue");
var Login_vue_1 = require("@/views/Login.vue");
var CreatePersonalSession_vue_1 = require("@/views/CreatePersonalSession.vue");
var AttendPersonalSession_vue_1 = require("@/views/AttendPersonalSession.vue");
var FeedbackView_vue_1 = require("@/components/FeedbackView.vue");
vue_1.default.use(vue_router_1.default);
var routes = [
    {
        path: '/',
        name: 'Home',
        component: Home_vue_1.default
    },
    {
        path: '/new-session',
        name: 'new session',
        component: NewSession_vue_1.default
    },
    {
        path: '/edit-session',
        name: 'edit session',
        component: EditSession_vue_1.default,
        props: true
    },
    {
        path: '/new-tutorial-target',
        name: 'new tutorial target',
        component: NewTutorialTarget_vue_1.default
    },
    {
        path: '/join-session/:participantId',
        name: 'join session',
        component: JoinSession_vue_1.default,
        props: true
    },
    {
        path: '/orchestrate-session',
        name: 'orchestrate session',
        component: OrchestrateSession_vue_1.default,
    },
    {
        path: '/attend-session',
        name: 'attend session',
        component: AttendSession_vue_1.default,
    },
    {
        path: '/login',
        name: 'login',
        component: Login_vue_1.default
    },
    {
        path: '/personal-session/create',
        name: 'Create Personal Session',
        component: CreatePersonalSession_vue_1.default
    },
    {
        path: '/personal-session/attend',
        name: 'Attend Personal Session',
        component: AttendPersonalSession_vue_1.default
    },
    {
        path: '/feedback',
        name: 'Feedback',
        component: FeedbackView_vue_1.default
    }
    // {
    //   path: '/about',
    //   name: 'About',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
];
var router = new vue_router_1.default({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes
});
exports.default = router;
//# sourceMappingURL=index.js.map