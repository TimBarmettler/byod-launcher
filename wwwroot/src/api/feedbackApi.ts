﻿import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import Api from "@/api/api";
import { Feedback } from "@/models/feedback";
import { Id } from "@/models/idType";


class FeedbackApi extends Api {

    public constructor(config: AxiosRequestConfig) {
        super(config);

        //this.getTargets = this.getTargets.bind(this);
    }

    public createFeedback(feedback: Feedback): Promise<Feedback> {
        return this.post<Feedback>('feedback', JSON.stringify(feedback))
            .then((response: AxiosResponse<Feedback>) => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw error;
            });
    }
}

export const feedbackApi = new FeedbackApi({});